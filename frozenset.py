# st=set()
# st.add(1)
# print(st)

# Frozen set
# fs=frozenset()
# fs.add(1)           #Give error bcz it is immutable
# print(fs)

fst=frozenset([1,2,2,2,3,3,4,4])
print(fst)
for i in fst:
    print(i)