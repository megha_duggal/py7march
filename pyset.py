# a={1,2,3,4,5,'python','django'}           #it is a set.set starts with curly brackets and have no key value pairs
# print(type(a))
# print(a)
# print(a[0])  #gives an error bcz set is an unordered collection of data

#Duplicacy not allowed in set
# a={1,2,3,4,5,6,6,6,6,6,7,8}
# print(a)

#Diff bw set and list
# a={1,2,3,4,5,'python','django','django'} 
# b=[1,2,3,4,5,'python','django','django']    
# print(a)
# print(b)

#Conversion
ls=[1,20,200,200,400,400,600]
convert=set(ls)
print(convert)

#Get all elements
for i in convert:
    print(i)